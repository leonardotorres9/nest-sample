#!/bin/bash
mkdir ~/.aws/
touch ~/.aws/credentials
printf "[eb-cli]\naws_access_key_id=%s\naws_secret_access_key=%s\n" "${AWS_ACCESS_KEY}" "${AWS_SECRETY_ACCESS_KEY}" >>~/.aws/credentials
touch ~/.aws/config
printf "[profile eb-cli]\nregion=us-east-1\noutput=json" >>~/.aws/config
cd dist
eb init ${APP_NAME} -r us-east-1
eb use nestsample-stage
eb setenv MONGODB_URI=${MONGODB_URI}
eb deploy nestsample-stage -l "C_${CI_COMMIT_SHA}" -m "Deploy from branch ${CI_COMMIT_REF_NAME}"
