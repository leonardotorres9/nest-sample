import * as dotenv from 'dotenv';
import * as fs from 'fs';

export class ConfigService {
  private readonly envConfig: { [key: string]: string };

  constructor(filePath: string) {
    const options = {
      path: filePath,
    };
    dotenv.config(options);
  }

  getMongoUri(): string {
    return process.env.MONGODB_URI;
  }
}
