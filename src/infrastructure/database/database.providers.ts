import * as mongoose from 'mongoose';
import { ConfigService } from '../config/config.service';

export const databaseProviders = [
  {
    provide: 'DbConnection',
    useFactory: async (config: ConfigService): Promise<typeof mongoose> => {
      const mongoUri = config.getMongoUri();
      if (!mongoUri) {
        throw new Error('MONGODB_URI env var undefined!');
      }
      const mongooseInstance = await mongoose.connect(config.getMongoUri(), { useNewUrlParser: true });
      return mongooseInstance;
    },
    inject: ['ConfigService'],
  },
];
