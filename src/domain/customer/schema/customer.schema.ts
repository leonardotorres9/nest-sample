import * as mongoose from 'mongoose';

export const CustomerSchema = new mongoose.Schema(
  {
    name: {
      type: String,
    },
    age: {
      type: Number,
    },
  },
  {
    timestamps: true,
  },
);
