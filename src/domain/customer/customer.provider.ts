import { Connection } from 'mongoose';
import { CustomerSchema } from './schema/customer.schema';

export const customerProviders = [
  {
    provide: 'customerDb',
    useFactory: (connection: Connection) => connection.model('Customers', CustomerSchema),
    inject: ['DbConnection'],
  },
];
