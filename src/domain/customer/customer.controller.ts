import { Controller, Get, Inject } from '@nestjs/common';
import { CustomerService } from './customer.service';

@Controller()
export class CustomerController {

  constructor(private readonly customerService: CustomerService) {
    // Please take note that this check is case sensitive!
  }

  @Get('/customers')
  async getHello(): Promise<any> {
    return this.customerService.create();
  }
}
