import { Module } from '@nestjs/common';
import { CustomerController } from './customer.controller';
import { CustomerService } from './customer.service';
import { DatabaseModule } from '../../infrastructure/database/database.module';
import { customerProviders } from './customer.provider';

@Module({
  imports: [DatabaseModule],
  controllers: [CustomerController],
  providers: [
    CustomerService,
    ...customerProviders,
  ],
  exports: [CustomerService],
})
export class CustomerModule { }
