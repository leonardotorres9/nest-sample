import { Inject, Injectable } from '@nestjs/common';
import { Customer } from './interface/customer.interface';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

@Injectable()
export class CustomerService {

  constructor(@Inject('customerDb') private readonly customerDB: Model<Customer>) { }

  async create(): Promise<Customer> {
    const customer: Customer = {
      name: 'João',
      age: 40,
    };
    return await this.customerDB.create(customer);
  }
}
