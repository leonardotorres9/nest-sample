import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CustomerModule } from './domain/customer/customer.module';
import { ConfigModule } from './infrastructure/config/config.module';

@Module({
  imports: [ConfigModule, CustomerModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
